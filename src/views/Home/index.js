import React from 'react'
// import PropTypes from 'prop-types'
import './home.css';
import NavBar from './components/NavBar';
import TrendingBar from "./components/TrendingBar";
import CarouselSection from "./components/CarouselSection";
import WelcomeCard from "./components/WelcomeCard";
import WelcomeTestimonial from "./components/WelcomeTestimonial";
import WelcomeFooter from "./components/WelcomeFooter";
import WelcomeFeature from "./components/WelcomeFeature";
import CustomerMail from "./components/CustomerMail";

const Home = props => {
    return (
        <div>
        <NavBar/>
        <TrendingBar/>
        <CarouselSection/>
        <WelcomeFeature/>
        <WelcomeCard/>
        <WelcomeTestimonial/>
        <CustomerMail/>
        <WelcomeFooter/>
        </div>
    )
}

Home.propTypes = {

}

export default Home
