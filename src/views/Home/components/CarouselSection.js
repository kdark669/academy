import React from "react";
const CarouselSection = props => {
    return <div className="carousel">
        <h1>Explore Endless Opportunities</h1>
        <div className="buttons">
            <button>Our Course</button>
            <button>Register Now</button>
        </div>
    </div>;
};
export default CarouselSection;