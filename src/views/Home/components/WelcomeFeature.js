import React from "react";

const WelcomeFeature = props => {
    return <section>
        <div className="title-content">
		<span className="header">
			Main Features/Benefits
		</span>
            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
        </div>
        <div className="feature-items">
            <div className="feature-item">
                <div className="feature-body">
                    <div className="round">
                        <div className="tick-circle"></div>
                    </div>
                    <div className="body-content">
                        <span className="sub-header">Unlimited Courses</span>
                        <span className="main-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem porro, repudiandae? Corporis cumque dignissimos dolorem dolores explicabo in minus molestias numquam! </span>

                    </div>
                </div>
                <button>View Courses</button>
            </div>
            <div className="feature-item">
                <div className="feature-body">
                    <div className="round">
                        <div className="tick-circle"></div>
                    </div>
                    <div className="body-content">
                        <span className="sub-header">Colaborate</span>
                        <span className="main-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem porro, repudiandae? Corporis cumque dignissimos dolorem dolores explicabo in minus molestias numquam! </span>

                    </div>
                </div>
                <button>Collaborate</button>
            </div>
            <div className="feature-item">
                <div className="feature-body">
                    <div className="round">
                        <div className="tick-circle"></div>
                    </div>
                    <div className="body-content">
                        <span className="sub-header">Share Resource</span>
                        <span className="main-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem porro, repudiandae? Corporis cumque dignissimos dolorem dolores explicabo in minus molestias numquam! </span>

                    </div>
                </div>
                <button>Share</button>
            </div>
            <div className="feature-item">
                <div className="feature-body">
                    <div className="round">
                        <div className="tick-circle"></div>
                    </div>
                    <div className="body-content">
                        <span className="sub-header">Teach with Us</span>
                        <span className="main-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem porro, repudiandae? Corporis cumque dignissimos dolorem dolores explicabo in minus molestias numquam! </span>

                    </div>
                </div>
                <button>Register</button>
            </div>
        </div>
    </section>;
};

export default WelcomeFeature;