import React from "react";

const WelcomeFooter = props => {
    return <footer>

        <div className="footer-items">
            <span className="sub-header">About Us</span>
            <span>Business</span>
            <span>Career</span>
            <span>Contact Us</span>
        </div>
        <div className="footer-items">
            <span className="sub-header">Courses</span>
            <span>Laravel</span>
            <span>Vue</span>
            <span>React</span>
        </div>
        <div className="footer-items">
            <span className="sub-header">Resources</span>
            <span>Articles</span>
            <span>Forums</span>
            <span>Blog</span>
            <span>Help</span>
        </div>
        <div className="footer-items">
            <span className="sub-header">Social</span>
            <span>Facebook</span>
            <span>Google</span>
            <span>Flutter</span>
            <span>Linkdin</span>
            <span>Youtube</span>
        </div>
    </footer>;
};

export default WelcomeFooter;