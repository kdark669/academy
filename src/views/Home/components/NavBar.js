import React from 'react';

const NavBar = (props) => {
    return (
        <div className="top-bar">
    <div className="top-bar-left">
        <div className="logo">LOGO</div>
        <div className="categories">
            <i className="fa fa-ballot"></i>
            <span>Categories</span>
        </div>
        <div className="search-bar">
            <input type="text" name="" placeholder="search anything"/>
                <i className="fa fa-search"></i>
        </div>
    </div>
    <div className="top-bar-right">
        <div className="bar-header">
            <span>Authorization</span>
        </div>
        <div className="bar-body">
            <span>Login</span>
            <span>Sign Up</span>
        </div>
    </div>
</div>);
};

export default NavBar;