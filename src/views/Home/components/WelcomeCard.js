import React from "react";

const WelcomeCard = props => {
    return <section className="welcome-card">
        <span className="header">How It Works</span>
        <span className="body-cards">
			<div className="body-card-item">
				<div className="card-header">Sign Up</div>
				<div className="card-body"></div>
			</div>
			<div className="body-card-item">
				<div className="card-header">Search Course</div>
				<div className="card-body"></div>
			</div>
			<div className="body-card-item">
				<div className="card-header">Get Enrolled</div>
				<div className="card-body"></div>
			</div>
			<div className="body-card-item">
				<div className="card-header">Enjoy Learning</div>
				<div className="card-body"></div>
			</div>
		</span>
    </section>;
};
export default WelcomeCard;