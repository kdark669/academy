import React from "react";

const WelcomeTestimonial = props => {
    return <section className="testimonials">
        <div className="header">Testimonials</div>
        <div className="testimonial-items">
            <div className="testimonial-item">
                <div className="profile">
                    <div className="avatar">
                        <img src="https://assets.change.org/photos/0/kx/ah/hjkxAHxAKbytEHg-800x450-noPad.jpg?1560309500" alt="Student avatar"/>
                    </div>
                    <div className="profile-detail">Prabhat Khadka, CFO, ITGLANCE</div>
                </div>
                <div className="testimonial">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
            </div>
            <div className="testimonial-item">
                <div className="profile">
                    <div className="avatar">
                        <img src="https://assets.change.org/photos/0/kx/ah/hjkxAHxAKbytEHg-800x450-noPad.jpg?1560309500" alt="Student avatar"/>
                    </div>
                    <div className="profile-detail">Prabhat Khadka, CFO, ITGLANCE</div>
                </div>
                <div className="testimonial">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
            </div>
        </div>
        <div className="associate-bar">
            <span>Teach With Us</span>
            <span>Collaborate With Us</span>
        </div>
    </section>;
};

export default WelcomeTestimonial;