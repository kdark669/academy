import React from "react";
const TrendingBar = props => {
    return <nav>
		<span className="header">
			Trending Categories
		</span>
    <div className="trendings">
        <div className="trendings-cat-item"><i className="fa fa-code"></i>Development</div>
        <div className="trendings-cat-item"><i className="fa fa-fax"></i>Business</div>
        <div className="trendings-cat-item"><i className="fa fa-microchip"></i>Technology</div>
        <div className="trendings-cat-item"><i className="fa fa-stamp"></i>Arts</div>
    </div>
</nav>
};
export default TrendingBar;