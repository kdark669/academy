import React from 'react'
import { Switch, Route } from 'react-router-dom'
// import PublicRoute from './PublicRoute';
// import PrivateRoute from './PrivateRoute';


const Routes = props => {
    const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

    //pages
    const Home = React.lazy(() => import('../views/Home'));
    const Auth = React.lazy(() => import('../views/Auth'));

    return (
        <React.Suspense fallback={loading()}>
            <Switch>
                <Route path="/" name="Home" component={Home} exact />
                <Route path="/getting-started" name="Auth" component={Auth} exact />
            </Switch>
        </React.Suspense>
    )
}

export default Routes
